%global debug_package %{nil}

Name:			openage
Version:		0.4.0
Release:			1%{?dist}
Summary:		Free (as in freedom) open source clone of the Age of Empires II engine
License:		GPL3+
URL:			http://openage.sft.mx
Source0:		https://github.com/SFTtech/openage/archive/v%{version}.tar.gz

BuildRequires:	clang
BuildRequires:	cmake
BuildRequires:	eigen3-devel
BuildRequires:	fontconfig-devel
BuildRequires:	gcc-c++
BuildRequires:	harfbuzz-devel
BuildRequires:	libepoxy-devel
BuildRequires:	libogg-devel
BuildRequires:	libopusenc-devel
BuildRequires:	libpng-devel
BuildRequires:	ncurses-devel
BuildRequires:	nyan
BuildRequires:	opusfile-devel
BuildRequires:	python3-Cython
BuildRequires:	python3-devel
BuildRequires:	python3-jinja2
BuildRequires:	python3-numpy
BuildRequires:	python3-pillow
BuildRequires:	python3-pygments
BuildRequires:	qt5-qtdeclarative-devel
BuildRequires:	qt5-qtquickcontrols
BuildRequires:	SDL2-devel
BuildRequires:	SDL2_image-devel
BuildRequires:	vulkan-headers
BuildRequires:	vulkan-loader-devel

Requires:		dejavu-fonts-common
Requires:		qt5-qtquickcontrols

%description
openage: a volunteer project to create a free engine clone of the Genie Engine used by Age of Empires, Age of Empires II (HD) and Star Wars: Galactic Battlegrounds.

%prep
%setup -n %{name}-%{version}

%build
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=%{_prefix} \
	-DCMAKE_INSTALL_LIBDIR=%{_libdir} \
	-DCMAKE_BUILD_TYPE=Release ..
make

%install
cd build
make DESTDIR=%{buildroot}/ install

%files
%{_sysconfdir}/openage/
%{_prefix}/lib/python*/
%{_libdir}/libopenage.so*
%{_datadir}/applications/openage.desktop
%{_datadir}/openage/
%{_datadir}/pixmaps/openage.svg
%{_bindir}/openage

%changelog
* Sun Aug 4 2019 - 0.4.0-1
- Initial spec file

