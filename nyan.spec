Name:			nyan
Version:		0
Release:			1%{?dist}
Summary:		Modding API with a typesafe hierarchical key-value database
License:		LGPL3+
URL:			https://github.com/SFTtech/nyan
Source0:		https://github.com/SFTtech/nyan/archive/master.zip

BuildRequires:	cmake
BuildRequires:	gcc-c++
BuildRequires:	flex

%description
Modding API with a typesafe hierarchical key-value database with inheritance and dynamic patching.

%prep
%setup -n nyan-master

%build
mkdir build
cd build
%cmake ..
make

%install
cd build
%make_install

%files
%{_libdir}/cmake/nyan/*
%{_libdir}/libnyan.so*
%{_includedir}/nyan/*
%{_bindir}/nyancat

%changelog
* Sun Aug 4 2019 - 0-1
- Initial spec file

